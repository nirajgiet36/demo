﻿using System;

namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
			// New line for first commit
			Console.WriteLine("How are you doing !");
			// Edited in the dev branch
			Console.WriteLine("dev branch added !");
			// Edited in the latest branch
			Console.WriteLine("latest branch added !");
			// Edited in the latest branch 2
			Console.WriteLine("latest branch 2 added !");
        }
    }
}
